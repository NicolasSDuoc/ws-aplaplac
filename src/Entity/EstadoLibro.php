<?php

namespace App\Entity;

use App\Repository\EstadoLibroRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EstadoLibroRepository::class)
 */
class EstadoLibro
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Libros::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $libro;

    /**
     * @ORM\Column(type="integer")
     */
    private $estado;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\Column(type="datetime")
     */
    private $FechaDevolucion;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $FechaDevolucionReal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibro(): ?Libros
    {
        return $this->libro;
    }

    public function setLibro(?Libros $libro): self
    {
        $this->libro = $libro;

        return $this;
    }

    public function getEstado(): ?int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getFechaDevolucion(): ?\DateTimeInterface
    {
        return $this->FechaDevolucion;
    }

    public function setFechaDevolucion(\DateTimeInterface $FechaDevolucion): self
    {
        $this->FechaDevolucion = $FechaDevolucion;

        return $this;
    }

    public function getFechaDevolucionReal(): ?\DateTimeInterface
    {
        return $this->FechaDevolucionReal;
    }

    public function setFechaDevolucionReal(?\DateTimeInterface $FechaDevolucionReal): self
    {
        $this->FechaDevolucionReal = $FechaDevolucionReal;

        return $this;
    }

    static $LIBRO_EST_DISPONIBLE = 1;
    static $LIBRO_EST_RESERVADO = 2;
    static $LIBRO_EST_PRESTADO = 3;

    
}
