<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use OpenApi\Annotations as OA;

class PagosController extends AbstractController
{
    /**
     * Este servicio realiza un pago a traves de webpay
     * @Route("/api/v1/pagos/realizarpago", name="app_pagos_pagospendientes", methods={"POST"})
     *  @OA\RequestBody(
     *    required=true,
     *    description="Datos a enviar a transbank",
     *    @OA\JsonContent(
     *       required={"idDocumento", "rut", "monto"},
     *       @OA\Property(property="idDocumento", type="string", example="6930660"),
     *       @OA\Property(property="rut", type="integer", example="20417319-2"),
     *       @OA\Property(property="monto", type="integer", example="1000"),
     *          )),
     * @OA\Response(
     *    response=201,
     *    description="Devuelve el token de la transaccion y una URL para realizar el pago y guarda un nuevo registro en Pagos_pendientes",
     *    @OA\JsonContent(
     *       @OA\Property(property="token", type="string", example="e9d555262db0f989e49d724b4db0b0af367cc415cde41f500a776550fc5fddd3"),
     *       @OA\Property(property="url", type="string", example="https://webpay3gint.transbank.cl/webpayserver/initTransaction"),
     *        )
     * ),
     * @OA\Response(
     *    response=500,
     *    description="Error interno del sistema",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Error interno del sistema")
     *        )
     *     )
     * ),
     * @OA\Response(
     *    response=400,
     *    description="JSON sin formato correcto",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Datos deben estar en formato JSON")
     *        )
     *     )
     * ),
     * @OA\Response(
     *    response=503,
     *    description="Error en el servicio de transbank",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Error enviado por transbank")
     *        )
     *     )
     * )
     * 
     * @OA\Tag(name="Pagos")
     */
    public function realizarPago(Request $request): Response
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }
        else{
            return $this->json(['status' => 'error', 'message' => 'Datos deben estar en formato JSON']);
        }

        return New JsonResponse(['token' => 'e9d555262db0f989e49d724b4db0b0af367cc415cde41f500a776550fc5fddd3',
                                 'url' => 'https://webpay3gint.transbank.cl/webpayserver/initTransaction' ], Response::HTTP_CREATED);
    }

    /**
     * Este servicio confirma el pago
     * @Route("/api/v1/pagos/confirmarpago/{rut}/{token}", name="app_pagos_confirmarpago", methods={"GET"})
     * @OA\Response(
     *    response=200,
     *    description="Devuelve el estado de la transaccion, si esta en estado autorizado, cambia el estado del pago a 'Pagado'",
     *    @OA\JsonContent(
     *       @OA\Property(property="status", type="string", example="AUTHORIZED"),
     *        )
     * ),
     * @OA\Response(
     *    response=500,
     *    description="Error interno del sistema",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Error interno del sistema")
     *        )
     *     )
     * ),
     * @OA\Response(
     *    response=404,
     *    description="No se encuentra Token o alumno en la bd",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="No se encuntra el token")
     *        )
     *     )
     * ),
     * @OA\Response(
     *    response=503,
     *    description="Error en el servicio de transbank",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Error enviado por transbank")
     *        )
     *     )
     * )
     * 
     * @OA\Tag(name="Pagos")
     */
    public function confirmarPago(Request $request, $rut, $token): Response
    {
        return New JsonResponse(['status' => 'AUTHORIZED'], Response::HTTP_CREATED);
    }

    /**
     * Este servicio crea un asiento contable en el ERP
     * @Route("/api/v1/pagos/crearasiento", name="app_pagos_crearasiento", methods={"POST"})
     * @OA\RequestBody(
     *    required=true,
     *    description="ID del documento obtenido del ERP y el rut del alumno",
     *    @OA\JsonContent(
     *       required={"idDocumento", "rut"},
     *       @OA\Property(property="idDocumento", type="string", example="6930660"),
     *       @OA\Property(property="rut", type="integer", example="20417319-2"),
     *          )),
     * @OA\Response(
     *    response=201,
     *    description="Se devuelve si el asiento fue creado correctamente",
     *    @OA\JsonContent(
     *       @OA\Property(property="status", type="string", example="success")
     *        )
     * ),
     * @OA\Response(
     *    response=500,
     *    description="Error interno del sistema",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Error interno del sistema")
     *        )
     *     )
     * ),
     * @OA\Response(
     *    response=404,
     *    description="No se encuentra el documento en el ERP",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="No se encuntra el documento")
     *        )
     *     )
     * ),
     * @OA\Response(
     *    response=503,
     *    description="Error en el servicio de ERP",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Error enviado por el ERP")
     *        )
     *     )
     * )
     * 
     * @OA\Tag(name="Pagos")
     */
    public function crearAsientoERP(Request $request): Response
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }
        else{
            return New JsonResponse(['message' => 'Datos deben estar en formato JSON'], Response::HTTP_NOT_FOUND);
        }
        return New JsonResponse(['status' => 'success'], Response::HTTP_CREATED);
    }

    /**
     * Este servicio devuelve el certificado de pago del ERP
     * @Route("/api/v1/pagos/buscarcertificado/{id}", name="app_pagos_buscardocumento", methods={"GET"})
     * @OA\Response(
     *    response=200,
     *    description="Se devuelve una url con el pdf",
     *    @OA\JsonContent(
     *       @OA\Property(property="url", type="string", example="http://aplaplac.cl/documentos/213479234.pdf")
     *        )
     * ),
     * @OA\Response(
     *    response=500,
     *    description="Error interno del sistema",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Error interno del sistema")
     *        )
     *     )
     * ),
     * @OA\Response(
     *    response=404,
     *    description="No se encuentra el documento en el ERP",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="No se encuntra el documento")
     *        )
     *     )
     * ),
     * @OA\Response(
     *    response=503,
     *    description="Error en el servicio de ERP",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Error enviado por el ERP")
     *        )
     *     )
     * )
     * 
     * @OA\Tag(name="Pagos")
     */
    public function buscarDocumento(Request $request, $id): Response
    {
        return New JsonResponse(['status' => 'success'], Response::HTTP_CREATED);
    }
}
