<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Libros;
use App\Entity\Matricula;
use App\Entity\EstadoLibro;

class LibrosController extends AbstractController
{
    /**
     * @Route("/api/v1/libros/getall", name="app_libros")
     */
    public function getAll(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $libros = $em->getRepository(Libros::class)->findAll();
        return $this->json(['status' => 'OK', 'response' => $libros]);
    }

    /**
     * @Route("/api/v1/libros/solicitar", name="app_libros_solicitar")
     */
    public function solicitarLibros(Request $request): Response
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }
        else{
            return $this->json(['status' => 'error', 'message' => 'data sent is not a JSON!']);
        }

        $libro = $request->get('idLibro');

        try{
            $em = $this->getDoctrine()->getManager();
            $usuario = $this->get('security.token_storage')->getToken()->getUser();
            $li = $em->getRepository(Libros::class)->findOneBy(['id' => $libro, 'estado' => \App\Entity\Libros::$LIBRO_EST_ACTIVO]);
            
            if(!is_null($li)){

                $li1 = $em->getRepository(EstadoLibro::class)->findBy(['libro' => $li, 'estado' => [\App\Entity\EstadoLibro::$LIBRO_EST_RESERVADO, \App\Entity\EstadoLibro::$LIBRO_EST_PRESTADO]]);
                if(empty($li1)){
                $li2 = new EstadoLibro();
                $li2->setLibro($li);

                if(in_array('ROLE_ALUMNO', $usuario->getRoles())){
                    $li2->setEstado(\App\Entity\EstadoLibro::$LIBRO_EST_RESERVADO);     
                }else{
                    $li2->setEstado(\App\Entity\EstadoLibro::$LIBRO_EST_PRESTADO);
                }

                $date = new \DateTime();

                $li2->setFecha($date);

                $li2->setFechaDevolucion($date->modify('+1 month'));

                $li2->setFechaDevolucionReal(null);

                $em->persist($li2);
                $em->flush();

                if(in_array('ROLE_ALUMNO', $usuario->getRoles())){
                    return $this->json(['status' => 'OK', 'message' => 'Se realizo la reserva de libro']);    
                }else{
                    return $this->json(['status' => 'OK', 'message' => 'Se realizo el prestamo de libro']);
                }
                
                }else{
                    return $this->json(['status' => 'error', 'message' => 'El libro solicitado esta reservado o en prestamo.']);
                }


            }else{
                return $this->json(['status' => 'error', 'message' => 'El libro solicitado no existe o no esta activo en el sistema.']);
            }


        }catch(Exception $e){
            return $this->json(['status' => 'error', 'message' => $e->getMessage()]);
        }

        $em = $this->getDoctrine()->getManager();
        $libros = $em->getRepository(Libros::class)->findAll();
        return $this->json(['status' => 'OK', 'response' => $libros]);
    }
}
