<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User;

class UserController extends AbstractController
{

    /**
     * @Route("/api/register", name="app_user_save")
     */
    public function save(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }
        else{
            return $this->json(['status' => 'error', 'message' => 'data sent is not a JSON!']);
        }
        
        $em = $this->getDoctrine()->getManager();

        $id = $request->request->get('id');
        $email = $request->request->get('email');
        $roles = $request->request->get('roles');
        $password = $request->request->get('password');

        if($id == null){
            $id = 0;
        }
        
        if($roles == null){
            return $this->json(['status' => 'ER','message'=>'Debe seleccionar Roles']);                
        }

        $user = $em->getRepository(User::class)->find($id);

        $userval = $em->getRepository(User::class)->findOneBy(['email' => $email]);

        if(!is_null($userval)){
            return new JsonResponse(['status' => 'ER', 'message' => 'Email ya existe en el sistema'], Response::HTTP_CREATED);
        }

        if(!is_object($user)){
            $user = new User();
            $newEncodedPassword = $passwordEncoder->encodePassword($user, $password);
            $user->setPassword($newEncodedPassword);
        }

        try {
            $user->setEmail($email)
            ->setRoles($roles);
            $em->persist($user);
            $em->flush();

            return new JsonResponse(['status' => 'OK', 'username' => $user->getEmail()], Response::HTTP_CREATED);
            
        }catch(DBALException $e){
            return new JsonResponse(['status' => 'ER','message'=>$e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        catch(\Exception $e){
            return new JsonResponse(['status' => 'ER','message'=>$e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);            
        }
    }
}
