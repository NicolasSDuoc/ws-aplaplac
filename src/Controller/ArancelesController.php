<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use OpenApi\Annotations as OA;

class ArancelesController extends AbstractController
{
    /**
     * Este servicio permite validar si un alumno tiene la matricula activa
     * @Route("/api/v1/matricula/validar/{rut}", name="app_matricula_valida", methods={"GET"})
     * @OA\Response(
     *    response=200,
     *    description="Devuelve OK cuando el alumno existe en el sistema y true si esta al dia o false si no esta al dia",
     *    @OA\JsonContent(
     *       @OA\Property(property="staus", type="boolean", example="true"),
     *       @OA\Property(property="message", type="string", example="Alumno con matricula al dia"),
     *        )
     *     )
     * ),
     * @OA\Response(
     *    response=404,
     *    description="No se encuentra el alumno en la base de datos",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="No se encuentra el alumno en el sistema")
     *        )
     *     )
     * ),
     * @OA\Response(
     *    response=500,
     *    description="Error interno del sistema",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Error interno del sistema")
     *        )
     *     )
     * ),
     * 
     * @OA\Response(
     *    response=400,
     *    description="Rut sin formato correcto",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="el rut debe estar en formato 11111111-1")
     *        )
     *     )
     * )
     * 
     * @OA\Tag(name="Aranceles")
     */
    public function matriculaValidar(Request $request, $rut): Response
    {

        

        return New JsonResponse(['message' => 'Alumno con matricula al dia'], Response::HTTP_OK);
    }


    /**
     * Este servicio devuelve los pagos pendientes del alumno
     * @Route("/api/v1/alumno/pagospendientes/{rut}", name="app_alumno_pagospendientes", methods={"GET"})
     * @OA\Response(
     *    response=200,
     *    description="Devuelve un JSON con los pagos pendientes del alumno",
     *    @OA\JsonContent(
     *       @OA\Property(property="1", type="array", 
     *          @OA\Items(
     *              @OA\Property(property="idDocumento", type="integer", example="6930660"),
     *              @OA\Property(property="monto", type="integer", example="1000"),
     *              @OA\Property(property="interes", type="integer", example="0"),
     *              @OA\Property(property="descuento", type="integer", example="0"),
     *              @OA\Property(property="fechaV", type="datetime", example="18/05/2021"),
     *              @OA\Property(property="sucursal", type="datetime", example="PAO"),
     *              @OA\Property(property="producto", type="datetime", example="matricula"),
     *                  )
     * 
     * ),
     * @OA\Property(property="2", type="array", 
     *          @OA\Items(
     *              @OA\Property(property="idDocumento", type="integer", example="6930661"),
     *              @OA\Property(property="monto", type="integer", example="1500"),
     *              @OA\Property(property="interes", type="integer", example="0"),
     *              @OA\Property(property="descuento", type="integer", example="0"),
     *              @OA\Property(property="fechaV", type="datetime", example="19/05/2021"),
     *              @OA\Property(property="sucursal", type="datetime", example="PAO"),
     *              @OA\Property(property="producto", type="datetime", example="arancel"),
     *                  )
     * 
     * ),
     *        )
     * ),
     * @OA\Response(
     *    response=404,
     *    description="No se encuentra el alumno en la base de datos",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="No se encuentra el alumnos en el sistema")
     *        )
     *     )
     * ),
     * @OA\Response(
     *    response=500,
     *    description="Error interno del sistema",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Error interno del sistema")
     *        )
     *     )
     * ),
     * 
     * @OA\Response(
     *    response=400,
     *    description="Rut sin formato correcto",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="el rut debe estar en formato 11111111-1")
     *        )
     *     )
     * )
     * 
     * @OA\Tag(name="Aranceles")
     */
    public function verPagosPendientes(Request $request, $rut): Response
    {
        return New JsonResponse(['1' => [
            'idDocumento' => '6930660',
            "monto" => "1000",
            "interes" => "0",
            "descuento" => "0",
            "fechaV" => "18/05/2021",
            "sucursal" => "PAO",
            "producto" => "matricula"
        ], '2' =>[
                'idDocumento' => '6930661',
                "monto" => "1500",
                "interes" => "0",
                "descuento" => "0",
                "fechaV" => "19/05/2021",
                "sucursal" => "PAO",
                "producto" => "arancel"
        ]], Response::HTTP_OK);
    }
}
