<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210630034522 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE estado_libro (id INT AUTO_INCREMENT NOT NULL, libro_id INT NOT NULL, estado INT NOT NULL, fecha DATETIME DEFAULT NULL, INDEX IDX_5D622BD9C0238522 (libro_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE libros (id INT AUTO_INCREMENT NOT NULL, nombre LONGTEXT NOT NULL, descr LONGTEXT DEFAULT NULL, estado INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matricula (id INT AUTO_INCREMENT NOT NULL, estado INT NOT NULL, monto INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE estado_libro ADD CONSTRAINT FK_5D622BD9C0238522 FOREIGN KEY (libro_id) REFERENCES libros (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE estado_libro DROP FOREIGN KEY FK_5D622BD9C0238522');
        $this->addSql('DROP TABLE estado_libro');
        $this->addSql('DROP TABLE libros');
        $this->addSql('DROP TABLE matricula');
    }
}
