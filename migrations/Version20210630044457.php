<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210630044457 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE matricula ADD alumno_id INT NOT NULL');
        $this->addSql('ALTER TABLE matricula ADD CONSTRAINT FK_15DF1885FC28E5EE FOREIGN KEY (alumno_id) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_15DF1885FC28E5EE ON matricula (alumno_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE matricula DROP FOREIGN KEY FK_15DF1885FC28E5EE');
        $this->addSql('DROP INDEX IDX_15DF1885FC28E5EE ON matricula');
        $this->addSql('ALTER TABLE matricula DROP alumno_id');
    }
}
